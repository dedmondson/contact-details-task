


/* New message block to inset */
const message="\
   <div class='custom-tooltip desktop-tooltip bg-color-light-blue'>\
     <div class='tooltip-box'>\
          <div class='color-cyan emailtooltip' >\
            <div><img  id='exp-message-icon' src='https://assets.bupa.co.uk/~/media/Images/decorative-images/hosted-images/bby-pink/Information-icon-20x20' >By providing your telephone number, you indicate you're happy for Bupa to contact you to discuss this quote and your health insurance options. We won't use your number for any other purpose.\
            </div>\
          </div>\
      </div>\
    </div>";


/* Clone the old form the remove all the existing listeners in one go*/
const oldForm = document.getElementById("formRegistration");
let   newForm = oldForm.cloneNode(true);
oldForm.parentNode.replaceChild(newForm, oldForm);


/* Insert the new message.. existing message will remain hidden as no longer triggered */
let newMessage  = document.createElement("div");
newMessage.setAttribute("id", "exp-message");
newMessage.classList.add("col-12");
newMessage.classList.add("d-lg-block");
newMessage.innerHTML= message;
let emailWrapper = newForm.querySelector(".email-field");
emailWrapper.parentNode.insertBefore(newMessage, emailWrapper.nextSibling);


/* Add new click action to next button to attempt to submit the form*/
let submitButton = newForm.querySelector("button.submit-btn");
submitButton.addEventListener("click", attemptSubmit);

/* Add a new listern for email blur for validation */
let emailField = document.getElementById("Prospect_ApplicantDetails_ContactDetails_EmailAddress");
emailField.addEventListener("blur", validateEmail);
emailField.addEventListener("focus", hideValidationError);


function showValidationError(){
	emailWrapper.classList.add("error");
}

function hideValidationError(){
	emailWrapper.classList.remove("error");
}


const regexEmail = /^(([^<>()[]\\.,;:\s@\"]+(\.[^<>()[]\\.,;:\s@\"]+)*)|(\".+\"))@(([[0-9]{1,3}\‌​.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; 

function validateEmail(){
	let isValid = true;
	
	if(!emailField.value || email.value.length < 4 || regexEmail.test(email) ){
	  isValid = false;	
	}
	
	if(!isValid){
		showValidationError();
	}

	return isValid;
}

function attemptSubmit(){
	if(validateEmail()){
	  newForm.submit(); 
	}

}